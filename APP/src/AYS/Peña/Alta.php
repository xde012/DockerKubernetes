<!DOCTYPE html>
<html>
<head>
<title>Ahora y Siempre</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" sizes="32x32" href="./img/favicon.png">

<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.0/css/foundation.min.css'>
<style class="cp-pen-styles">body {
  background-color: #f7f8f9;
}

#alta {
  border: 1px solid #cacaca;
  padding: 1rem;
  max-width: 400px;
  margin: 0 auto;
  background-color: white;
}
#alta .callout {
  display: none;
}
#alta .button {
  background-color: #00b3e6;
  font-weight: bold;
}
#alta .button:hover {
  background-color: #009fcd;
}
</style>
</head>
<body>

<form id="alta" method="post" action="./Registro.php">
  <h4 class="text-center">Añadir Nuevo Socio</h4>
  
  <label>
    DNI <span style="color:red">*</span>
    <input name="DNI" type="text">
  </label>
  
  <label>
    Nº Socio <span style="color:red">*</span>
    <input name="NSocio" type="text">
  </label>
  
  <label>
  Nombre <span style="color:red">*</span>
    <input name="Nombre" type="text">
  </label>
  
  <label>
  Apellidos <span style="color:red">*</span>
    <input name="Apellidos" type="text">
  </label>
  
  <label>
  Telefono <span style="color:red">*</span>
    <input name="Telefono" type="number">
  </label>
  
  <label>
  Domicilio <span style="color:red">*</span>
    <input name="Domicilio" type="text">
  </label>
  
  <label>
  Fecha Nacimiento <span style="color:red">*</span>
    <input name="FechaNacimiento" type="text" placeholder="YYYY-MM-DD">
  </label>
  
  <label>
  Correo
    <input name="Correo" type="email" placeholder="ejemplo@ejemplo.com">
  </label>
  
  <label>
  Domiciliación Bancaria
    <input name="Banco" type="text" placeholder="ES00-0000-0000-0000-0000-0000">
  </label>
  
  <input type="submit" class="button expanded" value="Registrar"></input><br>
  <input type="button" class="button expanded" onclick=location.href='./index.html'; value="Volver"></input>
  
</form>


</body>
</html>